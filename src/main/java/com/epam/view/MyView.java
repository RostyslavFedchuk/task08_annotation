package com.epam.view;

import com.epam.controller.ClassAnalyser;
import com.epam.model.Student;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    private ClassAnalyser classAnalyser;

    public MyView(){
        classAnalyser = new ClassAnalyser(new Student());
        menu = Menu.getMenu();
        setMenuMethods();
    }

    private void setMenuMethods(){
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::getSpecificFieldsInfo);
        menuMethods.put("2", this::getFieldsInfo);
        menuMethods.put("3", this::getMethodsInfo);
        menuMethods.put("4", this::getAllInfo);
        menuMethods.put("5", this::setFieldsValue);
        menuMethods.put("6", this::invokeMethods);
        menuMethods.put("Q", this::quit);
    }

    private void printMenu() {
        logger.info("__________________"
                + "REFLECTION MENU__________________");
        for (Map.Entry entry : menu.entrySet()) {
            logger.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        logger.info("__________________________________________________");
    }

    private void getFieldsInfo(){
        classAnalyser.getFieldsInfo();
    }

    private void getSpecificFieldsInfo(){
        classAnalyser.getSpecificFieldsInfo();
    }

    private void getMethodsInfo(){
        classAnalyser.getMethodsInfo();
    }

    private void setFieldsValue(){
        logger.info("Enter the name of the field and new value with whitespace:");
        SCANNER.nextLine();
        String[] input = SCANNER.nextLine().split(" ");
        if(input.length >=2){
            classAnalyser.setFieldsValue(input[0],input[1]);
        } else {
            logger.info("You should enter two strings!");
        }
    }

    private void invokeMethods(){
        classAnalyser.invokeMethods();
    }

    private void getAllInfo(){
        classAnalyser.showName();
        classAnalyser.getFieldsInfo();
        classAnalyser.getConstructors();
        classAnalyser.getMethodsInfo();
    }

    private void quit(){
        logger.info("Bye!");
    }

    public void show() {
        String option = "";
        do {
            printMenu();
            logger.info("Choose one option:");
            option = SCANNER.next().toUpperCase();
            if(menuMethods.containsKey(option)){
                menuMethods.get(option).print();
            }else {
                logger.info("Wrong input! Try again.");
            }
        } while (!option.equals("Q"));
    }
}
