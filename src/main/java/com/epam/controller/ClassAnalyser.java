package com.epam.controller;

import com.epam.model.annotations.Description;
import com.epam.model.Student;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class ClassAnalyser {
    private static Logger logger = LogManager.getLogger(ClassAnalyser.class);
    private Student student;
    private Class objClass;

    public ClassAnalyser(Object object) {
        if (nonNull(object)) {
            if (object instanceof Student) {
                this.student = (Student) object;
            }
            objClass = object.getClass();
            logger.info("Starting working with " + objClass.getName() + " instance");
        } else {
            logger.info("Cannot deal with nulls!");
        }
    }

    public void getSpecificFieldsInfo() {
        if (checker()) {
            return;
        }
        logger.info("\nFields with Description annotation in this object:");
        Arrays.stream(objClass.getDeclaredFields()).filter(f -> f.isAnnotationPresent(Description.class))
                .forEach(field -> {
                    logger.info(Modifier.toString(field.getModifiers()) + " "
                            + field.getType().getSimpleName() + " " + field.getName()
                            + ". Description: " + field.getAnnotation(Description.class).value());
                });
    }

    public void getFieldsInfo() {
        if (checker()) {
            return;
        }
        logger.info("\nInformation about all fields in this object:");
        Arrays.stream(objClass.getDeclaredFields()).forEach(field -> {
            logger.info(Modifier.toString(field.getModifiers()) + " "
                    + field.getType().getSimpleName() + " " + field.getName());
        });
    }

    public void getMethodsInfo() {
        if (checker()) {
            return;
        }
        logger.info("\nInformation about all methods in this object:");
        Arrays.stream(objClass.getDeclaredMethods()).forEach(method -> {
            logger.info(Modifier.toString(method.getModifiers()) + " "
                    + method.getReturnType().getSimpleName() + " " + method.getName()
                    + "(" + Arrays.stream(method.getParameterTypes()).map(Class::getSimpleName)
                    .collect(Collectors.joining(", ")) + ");");
        });
    }

    public void getConstructors() {
        logger.info("\nInformation about all constructors in this object:");
        Arrays.stream(objClass.getDeclaredConstructors())
                .forEach(c -> {
                    logger.info(Modifier.toString(c.getModifiers()) + " " + objClass.getSimpleName() + "("
                            + Arrays.stream(c.getParameterTypes())
                            .map(Class::getSimpleName)
                            .collect(Collectors.joining(", ")) + ");");
                });
    }

    public void showName() {
        logger.info(Modifier.toString(objClass.getModifiers()) + " class " + objClass.getSimpleName() + ":");
    }

    public void setFieldsValue(String fieldToSet, String newValue) {
        try {
            logger.info("\nStudent object before reflection: " + student);
            logger.info("We are trying to set new Value to the " + fieldToSet + " field:");
            Field field = objClass.getDeclaredField(fieldToSet);
            field.setAccessible(true);
            field.set(student, newValue);
            logger.info("Student object after reflection: " + student + "\n");
        } catch (NoSuchFieldException e) {
            logger.info("There is no such field!");
        } catch (IllegalAccessException e) {
            logger.info("Something went wrong - IllegalAccessException");
        } catch (Exception e) {
            logger.info("We could not do reflection!");
        }
    }

    public void invokeMethods() {
        if (checker()) {
            return;
        }
        try {
            invokeAllMethods();
        } catch (NoSuchMethodException e) {
            logger.info("NoSuchMethodException");
        } catch (IllegalAccessException e) {
            logger.info("IllegalAccessException");
        } catch (InvocationTargetException e) {
            logger.info("InvocationTargetException");
        }
    }

    private void invokeAllMethods() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        int countMethods = 4;
        Method[] methods = new Method[countMethods];
        methods[0] = objClass.getDeclaredMethod("setInfo");
        methods[1] = objClass.getDeclaredMethod("myMethod", String.class, int[].class);
        methods[2] = objClass.getDeclaredMethod("myMethod", String[].class);
        methods[3] = objClass.getDeclaredMethod("toString");
        for (int i = 0; i < countMethods; i++) {
            logger.info("Invoking " + methods[i].getName() + " method");
            if (i == 1) {
                methods[i].invoke(student, "a", new int[]{1, 2});
            } else if (i == 2) {
                methods[i].invoke(student, (Object) new String[]{"a", "bb"});
            } else {
                logger.info(methods[i].invoke(student));
            }
        }
    }

    private boolean checker() {
        if (isNull(objClass)) {
            logger.info("Cannot deal with nulls!");
            return true;
        } else {
            return false;
        }
    }
}
