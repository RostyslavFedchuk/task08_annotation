package com.epam.model;

import java.util.Scanner;

import com.epam.model.annotations.DateTime;
import com.epam.model.annotations.Description;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Student {
    private static Logger logger = LogManager.getLogger(Student.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    @Description(value = "Surname and initials.")
    private String name;
    @Description(value = "Full age.")
    private int age;
    @Description(value = "Where does the student come from.")
    private String nationality;
    private String institute;
    private int course;

    @DateTime(date = "9/26/2019", time = "10:55")
    public void setInfo(){
        logger.info("Enter name(ex: Smith J.W.), nationality," +
                " age, course and institute: ");
        name = SCANNER.nextLine();
        nationality = SCANNER.next();
        age = SCANNER.nextInt();
        course = SCANNER.nextInt();
        institute = SCANNER.next();
    }

    public int getOlder(){
        age++;
        return this.age;
    }

    public int getOlder(int age){
        this.age+=age;
        return this.age;
    }

    public void myMethod(String a, int ... args){
        logger.info("Invoked myMethod with String a, int... args");
    }

    public void myMethod(String... args){
        logger.info("Invoked myMethod with String... args");
    }

    public String toString() {
        return "Name: " + name + "; Age: " + age + ";Nationality: "
                + nationality + "; Education: "+ institute +"; Course: " + course;
    }
}
